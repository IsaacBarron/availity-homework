package com.isaaccode.util;

public interface CSVParser
{
    void collectHeadersAndValues();
    void parseValues();
    void parseHeaders();
    void parseRows();
}