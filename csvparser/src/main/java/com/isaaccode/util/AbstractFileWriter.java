package com.isaaccode.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractFileWriter
{
    private Map<String, BufferedWriter> openFiles = new HashMap<>();
    private String pathToDirectory;
    private String CSVHeader;

    AbstractFileWriter(String pathToDestination, String CSVHeader)
    {
        this.pathToDirectory = pathToDestination;
        this.CSVHeader = CSVHeader;
    }

    private void addToBufferMap(String fileName, BufferedWriter bufferedWriter)
    {
        this.openFiles.put(fileName, bufferedWriter);
    }

    public void write(String fileName, String text)
    {
        try
        {
            if (this.openFiles.containsKey(fileName))
            {
                BufferedWriter writer = this.openFiles.get(fileName);
                writer.write(text);
                writer.newLine();

            } else
            {
                File file = new File(this.pathToDirectory + fileName);

                // append header
                BufferedWriter bufferedWriter = createBuffer(file);
                this.addToBufferMap(fileName, bufferedWriter);
                bufferedWriter.write(this.CSVHeader);
                bufferedWriter.newLine();
                bufferedWriter.write(text);
                bufferedWriter.newLine();
            }
        } catch (IOException e)
        {
            System.out.println("Error writing to file " + fileName + ":  " + e.getMessage());
        }
    }

    private static BufferedWriter createBuffer(File file) throws IOException
    {
        System.out.println("created file " + file.getName());
        return new BufferedWriter(new FileWriter(file, true));
    }

    public void closeAllFiles()
    {
        this.openFiles.values().forEach(file ->
        {
            try
            {
                file.close();
            } catch (IOException e)
            {
                System.out.println("Error closing file:" + e.getMessage());
            }
        });
    }
}
