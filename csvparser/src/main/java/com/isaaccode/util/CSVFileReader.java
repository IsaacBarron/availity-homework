package com.isaaccode.util;

import com.isaaccode.Main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.isaaccode.constants.PathConstants.RESOURCE_PATH;


public class CSVFileReader
{
    private final String fileName;
    private String headers;
    private List<String> rows = new ArrayList<>();

    public CSVFileReader(String fileName)
    {
        this.fileName = fileName;
        parse();
    }

    private void parse()
    {
        BufferedReader reader;
        try
        {
            reader = new BufferedReader(
                    new FileReader(RESOURCE_PATH + fileName)
            );
            String line = reader.readLine(); // first like is headers

            // get Header row
            if (line != null)
            {
                headers = line;
                line = reader.readLine();
            }

            // Collect Rows
            while (line != null)
            {
                rows.add(line);
                // read next line
                line = reader.readLine();
            }

            reader.close();
        } catch (IOException e)
        {
            System.out.println("File name" + fileName + " doesn't exit in resource file : " + e.getMessage());
        }
    }

    public String getHeaders()
    {
        return headers;
    }

    public List<String> getRows()
    {
        return rows;
    }
}
