package com.isaaccode;

import com.isaaccode.util.AbstractFileWriter;
import com.isaaccode.util.CSVFileReader;
import com.isaaccode.util.CSVFileWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import static com.isaaccode.constants.PathConstants.RESOURCE_PATH;

/**
 * separate enrollees by insurance company in its own file. Additionally, sort the contents of each file by last and
 * first name (ascending).  Lastly, if there are duplicate User Ids for the same Insurance
 * Company, then only the record with the highest version should be included.
 */
public class Main
{
    public static void main(String[] args)
    {
        CSVFileReader fileReader = new CSVFileReader("clientData.csv");
        List<String> rows = fileReader.getRows();
        String header = fileReader.getHeaders();
        int userIdPosition = 0;
        int fullNamePosition = 1;
        int versionPosition = 2;
        int insuranceCompanyPosition = 3;

        Map<String, String[]> clientMap = new HashMap<>();
        rows.forEach(row ->
        {
            String[] properties =
                    row.replaceAll("\\s+[\\s,]\\s+|\\s+,|,\\s+", ",") // clear data
                    .split(","); // separate values

            // if user in map
            if (clientMap.containsKey(properties[userIdPosition]))
            {
                String[] savedClientProperties = clientMap.get(properties[userIdPosition]);
                // check versions
                if(Integer.parseInt(savedClientProperties[versionPosition]) < Integer.parseInt(properties[versionPosition]))
                {
                    System.out.println("Replacing " + savedClientProperties[versionPosition] + " with " + properties[versionPosition] );
                    clientMap.put(properties[userIdPosition], properties);
                }
            } else {
                clientMap.put(properties[userIdPosition], properties);
            }
        });

        // create list and sort by name ascending
        List<String[]> clients = new ArrayList<>();
        clientMap.forEach((key, value) -> clients.add(value));

        // sort
        clients.sort(Comparator.comparing((String[] o) -> o[fullNamePosition]));


        // save by insurance company name
        AbstractFileWriter fileWriter = new CSVFileWriter(RESOURCE_PATH, header);

        clients.forEach(client -> {
            String fileName = client[insuranceCompanyPosition].replace(" ", "-") + ".csv";

            // format properties back to csv
            String entry = String.join(",", client);
            fileWriter.write(fileName, entry);
        });

        // close all files
        fileWriter.closeAllFiles();
    }
}
