package com.isaaccode.validator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateLISP
{
    private String lispString;

    public ValidateLISP(String lispString)
    {
        this.lispString = lispString;
    }

    public void validate()
    {
        List<String> allMatches = extractParentheses(this.lispString);
        this.checkSyntax(allMatches);
    }

    private void checkSyntax(List<String> stringOfParenthesesOnly)
    {
        Iterator iterator = stringOfParenthesesOnly.iterator();
        int leftParentheseCount = 0;
        int rightParentheseCount = 0;
        int expressionSet = 1;
        while (iterator.hasNext())
        {
            String current = (String) iterator.next();
            if (current.equals("("))
            {
                leftParentheseCount++;
            } else if (current.equals(")"))
            {
                rightParentheseCount++;
            }

            if (leftParentheseCount == rightParentheseCount) // found a set
            {
                expressionSet++;
                leftParentheseCount = 0;
                rightParentheseCount = 0;
            }
        }

        if (leftParentheseCount != rightParentheseCount)
        {
            System.out.println("Incorrect syntax");
        } else {
            System.out.println(expressionSet + " expressions found");
        }
    }

    private static List<String> extractParentheses(String lispString)
    {

        List<String> allMatches = new ArrayList<>();
        final Matcher m = Pattern.compile("([()])")
                .matcher(lispString);
        while (m.find())
        {
            allMatches.add(m.group());
        }

        return allMatches;
    }
}
