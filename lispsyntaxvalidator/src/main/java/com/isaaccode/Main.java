package com.isaaccode;

import com.isaaccode.validator.ValidateLISP;

import java.util.Scanner;

public class Main
{
    // take in lisp string
    // validate that parentheses in the string are properly closed and nested
    public static void main(String[] args)
    {
        // get input string
        Scanner scanner = new Scanner(System.in);

        String lipsString = scanner.nextLine();

        ValidateLISP validateLISP = new ValidateLISP(lipsString);
        validateLISP.validate();
    }
}
