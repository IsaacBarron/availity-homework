export const nameValidator = (event, updateErrorMessage, updateForm) => {
    const input = event.target.value;
    const formId = event.target.name;
    const message = 'No spaces, number or special characters';
    let error = false;

    if (/(\W|\d)/.test(input) && input.length > 0) {
        updateErrorMessage(true, message);
        error = true;
    } else {
        updateErrorMessage(false, '');
    }
    updateForm(input, formId, error);
};

export const emailValidator = (event, updateErrorMessage, updateForm) => {
    const input = event.target.value;
    const formId = event.target.name;
    const message = 'Invalid email';
    let error = false;

    // found regex online has over 4500 votes seemed pretty legit
    // https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
    if (!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(input) && input.length > 0) {
        updateErrorMessage(true, message);
        error = true;
    } else {
        updateErrorMessage(false, '');
    }
    updateForm(input, formId, error);
};

export const tenDigitValidator = (event, updateErrorMessage, updateForm) => {
    const input = event.target.value;
    const formId = event.target.name;
    const message = 'Must be 10 digits';
    let error = false;

    if (!/\d{10}/.test(input) && input.length > 0) {
        updateErrorMessage(true, message);
        error = true;
    } else {
        updateErrorMessage(false, '');
    }
    updateForm(input, formId, error);
};

export const addressValidator = (event, updateErrorMessage, updateForm) => {
    const input = event.target.value;
    const formId = event.target.name;
    const message = 'Invalid street address ex. 61 Park Street, Camden, ME, 04843';
    let error = false;

    if (!/^\d+\s[A-z]+\s[A-z]+,\s[A-z]+,\s[A-z]+,\s\d{5}/.test(input) && input.length > 0) {
        updateErrorMessage(true, message);
        error = true;
    } else {
        updateErrorMessage(false, '');
    }
    updateForm(input, formId, error);
};

