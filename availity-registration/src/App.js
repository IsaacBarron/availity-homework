import React from 'react';
import './App.css';
import Registration from "./components/registration/Registration";

const App = () => {
  return (
    <div id='main-container' className="d-flex align-items-center flex-column justify-content-center ">
      <Registration/>
    </div>
  );
};

export default App;
