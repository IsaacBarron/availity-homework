import React, {useState} from "react";
import Input from "./Input";
import {
    addressValidator,
    emailValidator,
    nameValidator,
    tenDigitValidator
} from "../../util/input-validators";

const Registration = () => {

    const [form, setFormValue] = useState({
        firstName: '',
        lastName: '',
        NPINumber: '',
        businessAddress: '',
        phone: '',
        email: '',
        errors: {
            firstName: false,
            lastName: false,
            NPINumber: false,
            businessAddress: false,
            phone: false,
            email: false,
        }
    });

    const updateForm = (input, formId, error) => {
        let newErrors = {...form.errors};
        if (error) {
            if (!newErrors[formId]) {
                newErrors = {
                    ...form.errors,
                    [formId]: true
                };
            }
        } else if (newErrors[formId])
        {
            newErrors = {
                ...form.errors,
                [formId]: false
            };
        }

        setFormValue({
            ...form,
            [formId]: input,
            errors: newErrors
        });
    };

    const confirmFormComplete = () => {
        let errors = {...form.errors};
        const foundErrors = Object.values(errors).filter(error => error);

        return (
            form.firstName !== '' &&
            form.lastName !== '' &&
            form.email !== '' &&
            form.phone !== '' &&
            form.businessAddress !== '' &&
            form.NPINumber !== '' &&
            foundErrors.length === 0
        )
    };

    return (
        <div id="login-container" className="w-75 h-auto shadow-sm p-3 mb-5 bg-white rounded"
             style={{maxWidth: '500px'}}>
            <div className="row">
                <h2 className="w-100 text-center">Registration</h2>
            </div>
            <div className="row">
                <Input
                    inputType="text"
                    id="firstName"
                    placeHolder="First Name"
                    updateForm={updateForm}
                    validator={nameValidator}
                    colSize="col-12"
                />
                <Input
                    inputType="text"
                    id="lastName"
                    placeHolder="Last Name"
                    updateForm={updateForm}
                    validator={nameValidator}
                    colSize="col-12"
                />
                <Input
                    inputType="text"
                    id="NPINumber"
                    placeHolder="NPI number"
                    updateForm={updateForm}
                    validator={tenDigitValidator}
                    colSize="col-12"
                />
                <Input
                    inputType="text"
                    id="businessAddress"
                    placeHolder="Business Address"
                    updateForm={updateForm}
                    validator={addressValidator}
                    colSize="col-12"
                />
                <Input
                    inputType="text"
                    id="phone"
                    placeHolder="Phone"
                    updateForm={updateForm}
                    validator={tenDigitValidator}
                    colSize="col-12"
                />
                <Input
                    inputType="text"
                    id="email"
                    placeHolder="Email"
                    updateForm={updateForm}
                    validator={emailValidator}
                    colSize="col-12"
                />
            </div>
            <button
                type="submit"
                className={'btn w-100 ' + (confirmFormComplete() ? 'btn-success' : 'btn-danger disabled')}
                disabled={!confirmFormComplete()}
                onClick={() => console.log("send data", form)}
            >
                Submit
            </button>
        </div>
    );
};

export default Registration;