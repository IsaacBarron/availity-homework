import React, {useState} from "react";

const Input = ({id, colSize, inputType, placeHolder, validator, updateForm}) => {

    const [error, setError] = useState({
        isError: false,
        message: ''
    });

    const updateErrorMessage = (invalidInput, message) => {
        if (invalidInput) {
            setError({
                isError: invalidInput,
                message
            })
        }
        // Make sure not to update state if error on input state
        // already false
        else if (!invalidInput && error.isError) {
            setError({
                isError: false,
                message: ''
            })
        }
    };

    const handleInputChange = (event) => {
        validator(event, updateErrorMessage, updateForm)
    };

    const showErrorMessage = () => {
        return error.isError &&
            <div className="alert alert-danger" role="alert">
                {error.message}
            </div>
    };

    return (
        <div className={"form-group " + colSize}>
            {showErrorMessage()}
            <input type={inputType}
                   className="form-control"
                   name={id}
                   id={id}
                   placeholder={placeHolder}
                   onChange={handleInputChange}/>
        </div>
    );
};

export default Input;